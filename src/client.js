const WebSocket = require("ws")
const http = require("http")
const { generateRandomInt, generateRandomString, objHas, objHasAll } = require("./functions");


/**
 * @type Map<string, Client>
 * @type Map<string, string>
 */

const clients = new Map();
const rooms = new Map();


/**
 * Class representing a client
 */
class Client {
    /**
     * Create a client
     * @param {string} id String representing the client
     * @param {WebSocket} ws WebSocket connection
     */
    constructor(id, ws) {
        this.id = id;
        this.ws = ws;
        this.roomID = "";
    }

    delete() {
        rooms.delete(this.roomID);
        clients.delete(this.id);
    }

    sendObject(obj) {
        if (typeof obj !== "object") {
            return;
        }

        this.ws.send(JSON.stringify(obj));
    }

    createRoom() {
        rooms.delete(this.roomID);
        
        let roomID = generateRandomString(8);
        while (rooms.has(roomID)) {
            roomID = generateRandomString(8);
        }

        rooms.set(roomID, this.id);
        this.roomID = roomID;

        this.sendObject({
            type: "room_created",
            room_id: roomID
        });
    }

    removeRoom() {
        rooms.delete(this.roomID);
        this.roomID = "";
    }

    joinRoom(room_id) {
        if (!rooms.has(room_id)) {
            this.sendObject({
                type: "join_request_refused",
                reason: "No room found with this ID!"
            });
            return;
        } 
        else if(!clients.has(rooms.get(room_id))) {
            rooms.delete(room_id);
            this.sendObject({
                type: "join_request_refused",
                reason: "No room found with this ID!"
            });
            return;
        }

        clients.get(rooms.get(room_id)).sendObject({
            type: "join_request",
            sig_client_id: this.id
        });
    }

    onJoinAccepted(sig_client_id, host_peer_id, client_peer_id) {
        if (this.roomID === "") { 
            return; 
        }
        else if (!clients.has(sig_client_id)) {
            return;
        }

        clients.get(sig_client_id).sendObject({
            type: "join_request_accepted",
            sig_client_id: this.id,
            room_id: this.roomID,
            host_peer_id: host_peer_id,
            client_peer_id, client_peer_id
        });
    }

    onJoinRefused(sig_client_id, reason) {
        if (!clients.has(sig_client_id)) {
            return;
        }

        clients.get(sig_client_id).sendObject({
            type: "join_request_refused",
            room_id: this.roomID,
            reason: reason
        });
    }

    sendSignal(info) {
        if (!clients.has(info.sig_client_id)) {
            return;
        }

        const c = clients.get(info.sig_client_id);

        info.type = "signal_received";
        info.sig_client_id = this.id;


        c.sendObject(info);
    }

}


/**
 * Handle new websocket connections
 * @this {WebSocket.Server} WebSocket.Server
 * @param {WebSocket} ws websocket connection
 * @param {http.IncomingMessage} req Incoming request
 */
function onWsConnection(ws, req) {
    let id = generateRandomInt();
    while (clients.has(id)) {
        id = generateRandomInt();
    }

    const client = new Client(id, ws);
    clients.set(id, client);

    console.log(`Client connected with id ${id}`);


    ws.on("message", (m) => {
        if (typeof m !== "string") {
            ws.close(1003, "Unexpected data");
            client.delete();
        }

        let p;

        try {
            p = JSON.parse(m);
            if (typeof p !== "object") {
                throw new Error("JSON is not an object.");
            }
        } catch (e) {
            return;
        }

        if (!p.hasOwnProperty('type')) {
            return;
        }

        switch (p.type) {
            case "create_room":
                client.createRoom();
                break;
            
            case "remove_room":
                client.removeRoom();
                break;
            
            case "join_room":
                if (objHasAll(p, ["room_id"])) {
                    client.joinRoom(p.room_id);
                }
                break;
            
            case "join_accept":
                if (objHasAll(p, ["sig_client_id", "host_peer_id", "client_peer_id"])) {
                    client.onJoinAccepted(p.sig_client_id, p.host_peer_id, p.client_peer_id);
                }
                break;
            
            case "join_refuse":
                if (objHasAll(p, ["sig_client_id"])) {
                    client.onJoinRefused(p.sig_client_id, p.hasOwnProperty("reason") ? p.reason : "Host refused the connection");
                }
                break;
            
            case "send_signal":
                if (objHasAll(p, ["signal_type", "sig_client_id", "from_peer", "to_peer", "object"])) {
                    client.sendSignal(p);
                }
                break;
            
            default:
                break;
        }
    });


    ws.on("close", (code, reason) => {
        client.delete()
        console.log(`Closed connection with client ${id}. code ${code}, reason ${reason}`);
    });


    ws.on("error", (error) => {
        console.log(`Client ${id} error: ${error}`)
    });
}


module.exports = { onWsConnection };