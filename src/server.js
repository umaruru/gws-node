const WebSocket = require("ws");
const http = require("http");
const express = require("express");
const { onWsConnection } = require("./client");


const PORT = process.env.PORT || 3000;


const app = express();

app.get("/get/stats", (req, res) => {
    res.json({
        "connected": 12,
        "rooms": 3,
        "successfull": 125,
        "failed": 42,
        "total-signals": 412,
    });
});

app.use(express.static(__dirname + "/public"));
// app.get("/stats", (req, res) => {
//     res.sendFile(__dirname + "/public/stats.html");
// });

app.get("*", (req, res) => {
    res.sendFile(__dirname + "/public/index.html");
});


const server = http.createServer(app)
const wss = new WebSocket.Server({ server: server, path: "/sig" });
server.listen(PORT);


server.on("listening", () => {
    console.log("HTTP server listening on port " + PORT);
});

server.on("error", (error) => {
    console.log("HTTP server error!", error);
});


wss.on("connection", onWsConnection);

wss.on("error", (error) => {
    console.log("WebSocket Server error!", error);
})

wss.on("listening", () => {
    console.log("Signaling Server listening on port " + PORT)
});