window.onload = () => {
    fetch("/get/stats")
    .then(response => {
        const contentType = response.headers.get("content-type");
        if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json()
            .then(json => {
                if (typeof json !== "object") {
                    console.log("Didn't get an object");
                    return
                }
                for (let p in json) {
                    const e = document.getElementById(p);
                    e.innerText = json[p];
                }
            }) 
        }
        else {
            console.log("Didn't get a JSON!");
        }
    });
}