/**
 * Generates a random 32-bit signed integer (from 1 to 2^31)
 * @returns {number}
 */
function generateRandomInt() {
    const maxval = Math.pow(2, 31);
    return 1 + Math.floor(Math.random() * maxval);
}


/**
 * Generates a random string of the given size
 * @param {number} [size=8]
 * @returns {string}
 */
function generateRandomString(size = 8) {
    if (size <= 0) {
        return "";
    }

    const chars = "0123456789ABCDEF"
    let s = "";
    
    for (let i = 0; i < size; i ++) {
        s += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    
    return s;
}


/** 
 * Checks if object has all properties
 * @param {object} obj object
 * @param {string[]} props array of properties
 * @returns {boolean}
 */
function objHasAll(obj, props) {
    if (typeof obj !== "object") {
        return false;
    }

    for (let p of props) {
        if (typeof p !== "string") {
            continue;
        }

        if (!obj.hasOwnProperty(p)) {
            return false;
        }
    }

    return true;
}


/** 
 * Checks if object has a property
 * @param {object} obj object
 * @param {string} prop property
 * @returns {boolean}
 */
function objHas(obj, prop) {
    return (typeof obj === "object") && (obj.hasOwnProperty(prop));
}


module.exports = { generateRandomInt, generateRandomString, objHasAll, objHas };